package torresdehanoi;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelHanoi extends JPanel implements ActionListener, Runnable {

    private static Color BACKGROUND_COLOR = Color.RED;
    private static Color BORDER_COLOR = new Color(100, 0, 0);
    private static Color DISK_COLOR = Color.WHITE;
    private static Color MOVE_DISK_COLOR = Color.BLACK;
    private BufferedImage OSC;
    private int status;
    private static final int GO = 0;
    private static final int PAUSE = 1;
    private static final int STEP = 2;
    private static final int RESTART = 3;

    private int[][] torre;
    private int[] towerHeight;
    private int moveDisk;
    private int moveTower;
    private int discos;
    private Ventana ventana;

    private JButton runPauseButton;
    private JButton nextStepButton;
    private JButton startOverButton;

    public PanelHanoi(int discos) {
        this.discos = discos;
        OSC = new BufferedImage(430, 148, BufferedImage.TYPE_INT_RGB);
        ventana = new Ventana();
        ventana.setPreferredSize(new Dimension(430, 148));
        ventana.setBorder(BorderFactory.createLineBorder(BORDER_COLOR, 2));
        ventana.setBackground(BACKGROUND_COLOR);
        setLayout(new BorderLayout());
        add(ventana, BorderLayout.CENTER);
        JPanel buttonBar = new JPanel();
        add(buttonBar, BorderLayout.SOUTH);
        buttonBar.setLayout(new GridLayout(1, 0));
        runPauseButton = new JButton("Iniciar");
        runPauseButton.addActionListener(this);
        buttonBar.add(runPauseButton);
        nextStepButton = new JButton("Siguiente Paso");
        nextStepButton.addActionListener(this);
        buttonBar.add(nextStepButton);
        startOverButton = new JButton("Reiniciar");
        startOverButton.addActionListener(this);
        startOverButton.setEnabled(false);
        buttonBar.add(startOverButton);
        new Thread(this).start();
    }

    private class Ventana extends JPanel {

        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            int x = (getWidth() - OSC.getWidth()) / 2;
            int y = (getHeight() - OSC.getHeight()) / 2;
            g.drawImage(OSC, x, y, null);
        }
    }

    synchronized public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == runPauseButton) {  
            if (status == GO) {  
                status = PAUSE;
                nextStepButton.setEnabled(true);
                runPauseButton.setText("Iniciar");
            } else {  
                status = GO;
                nextStepButton.setEnabled(false); 
                runPauseButton.setText("Pausar");
            }
        } else if (source == nextStepButton) { 
            status = STEP;
        } else if (source == startOverButton) { 
            status = RESTART;
        }
        notify(); 
    }

    public void run() {
        while (true) {
            runPauseButton.setText("Iniciar");
            nextStepButton.setEnabled(true);
            startOverButton.setEnabled(false);
            setUpProblem();  
            status = PAUSE;
            checkStatus(); 
            startOverButton.setEnabled(true);
            try {
                solve(discos, 0, 2, 1);
            } catch (IllegalStateException e) {
                
            }
        }
    }

    synchronized private void checkStatus() {
        while (status == PAUSE) {
            try {
                wait();
            } catch (InterruptedException e) {
            
            }
        }
        if (status == RESTART) {
            throw new IllegalStateException("Reiniciar");
        }
        
    }

    synchronized private void setUpProblem() {
        moveDisk = 0;
        torre = new int[3][discos];
        for (int i = 0; i < discos; i++) {
            torre[0][i] = discos - i;
        }
        towerHeight = new int[discos];
        towerHeight[0] = discos;
        if (OSC != null) {
            Graphics g = OSC.getGraphics();
            drawCurrentFrame(g);
            g.dispose();
        }
        ventana.repaint();
    }

    private void solve(int disks, int from, int to, int spare) {
        if (disks == 1) {
            moveOne(from, to);
        } else {
            solve(disks - 1, from, spare, to);
            moveOne(from, to);
            solve(disks - 1, spare, to, from);
        }
    }

    synchronized private void moveOne(int fromStack, int toStack) {
        moveDisk = torre[fromStack][towerHeight[fromStack] - 1];
        moveTower = fromStack;
        delay(200);
        towerHeight[fromStack]--;
        putDisk(MOVE_DISK_COLOR, moveDisk, moveTower);
        delay(160);
        putDisk(BACKGROUND_COLOR, moveDisk, moveTower);
        delay(160);
        moveTower = toStack;
        putDisk(MOVE_DISK_COLOR, moveDisk, moveTower);
        delay(160);
        putDisk(DISK_COLOR, moveDisk, moveTower);
        torre[toStack][towerHeight[toStack]] = moveDisk;
        towerHeight[toStack]++;
        moveDisk = 0;
        if (status == STEP) {
            status = PAUSE;
        }
        if(towerHeight[2] == discos){
            status = PAUSE;
            runPauseButton.setText("Iniciar de Nuevo");
        }
        checkStatus();
    }

    synchronized private void delay(int milliseconds) {
        try {
            wait(milliseconds);
        } catch (InterruptedException e) {
        }
    }

    private void putDisk(Color color, int disk, int t) {
        Graphics g = OSC.getGraphics();
        g.setColor(color);
        g.fillRoundRect(75 + 140 * t - 5 * disk - 5, 116 - 12 * towerHeight[t], 10 * disk + 10, 10, 10, 10);
        g.dispose();
        ventana.repaint();
    }

    synchronized private void drawCurrentFrame(Graphics g){
        g.setColor(BACKGROUND_COLOR);
        g.fillRect(0, 0, 430, 143);
        g.setColor(BORDER_COLOR);
        if (torre == null) {
            return;
        }
        g.fillRect(10, 128, 130, 5);
        g.fillRect(150, 128, 130, 5);
        g.fillRect(290, 128, 130, 5);
        g.setColor(DISK_COLOR);
        for (int t = 0; t < 3; t++) {
            for (int i = 0; i < towerHeight[t]; i++) {
                int disk = torre[t][i];
                g.fillRoundRect(75 + 140 * t - 5 * disk - 5, 116 - 12 * i, 10 * disk + 10, 10, 10, 10);
            }
        }
        if (moveDisk > 0) {
            g.setColor(MOVE_DISK_COLOR);
            g.fillRoundRect(75 + 140 * moveTower - 5 * moveDisk - 5, 116 - 12 * towerHeight[moveTower],
                    10 * moveDisk + 10, 10, 10, 10);
        }
    }
}
