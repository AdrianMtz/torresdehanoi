package torresdehanoi;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TorresDeHanoi {
    public static void main(String[] args) {
      JFrame f = new JFrame("Torres de Hanoi");
      int discos = 0;
      do{
            discos = Integer.parseInt(
                JOptionPane.showInputDialog("Introduce la cantidad de discos."));
            if(discos < 3 || discos > 10){
                JOptionPane.showMessageDialog(null, "Error la cantidad de discos debe estar"
                        + " en un rango del 3 al 10.", "Error", JOptionPane.ERROR_MESSAGE);
            }
      }while(discos < 3 || discos > 10);
      f.setContentPane(new PanelHanoi(discos));
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.pack();
      f.setResizable(true);
      f.setLocation(300,200);
      f.setVisible(true);
    }
    
}
